<?php

class TripletexWebformTestCase extends WebformTestCase {

  /**
  * Users
  */
  protected $tripletex_admin, $tripletex_user;

  /**
  * getInfo() returns properties that are displayed in the test selection form.
  */
  public static function getInfo() {
    return array(
        'name' => 'Tripletex Webform',
        'description' => 'Testing the Tripletex Webform integration.',
        'group' => 'Tripletex',
    );
  }

  /**
   * setUp() performs any pre-requisite tasks that need to happen.
   */
  public function setUp() {
    // Enable any modules required for the test.
    parent::setUp(array('tripletex', 'tripletex_webform', 'ctools', 'views', 'views_ui', 'tripletex_dummy_api'));

    // Check login without config data
    $data = array();
    $data['tripletex_user'] = 'test@test.com';
    $data['tripletex_pass'] = 'userPass';
    $data['tripletex_syncsyst'] = '100';
    $data['tripletex_syncsyst_pass'] = 'sysPass';

    $this->tripletex_user = $this->drupalCreateUser( array('access tripletex content'));

    // Create and log in our privileged user.
    $this->tripletex_admin = $this->drupalCreateUser( array(
      'administer tripletex',
      'access tripletex content',
      'administer views',
    )
    );
    $this->drupalLogin($this->tripletex_admin);
    $this->drupalPost('admin/config/services/tripletex/credentials', $data, 'Save configuration');
    $this->assertText(t('Login succeeded!'), t('Admin login with correct detials success'));

  }

  /**
   * Retreives the cid from an url
   *
   * @param string $url
   */
  public function get_cid($url) {
    return array_pop(explode('=', $url));
  }



}

class  TripletexWfSettingsTestCase extends TripletexWebformTestCase {

  /**
  * getInfo() returns properties that are displayed in the test selection form.
  */
  public static function getInfo() {
    return array(
        'name' => 'Tripletex Webform Settings',
        'description' => 'Testing the Tripletex Webform integration settings.',
        'group' => 'Tripletex',
    );
  }

  public function testTripletexWebformSettings() {

    $this->testWebformForm();
    $this->drupalGet("node/1");
    $this->assertResponse(200, t('Authorized user is not allowed to access invoice creation page.'));

    $this->drupalLogin($this->webform_users['admin']);
    $this->drupalGet("admin/modules");

    $this->drupalGet("node/1/webform");
    $this->drupalGet("node/1/webform/conditionals");
    $this->drupalGet("node/1/webform/emails");
    $this->drupalGet("node/1/webform/configure");

    // Add priced component to the form
    $select = array('add[name]' => 'Price-component', 'add[type]'=>'select');
    $this->drupalPost("node/1/webform", $select, 'Add');
    $this->pass('<pre>' . print_r($this->getUrl(), TRUE) . '</pre>');

    $select = array('extra[items]' => '200|Expensive
    30|Cheap');
    $this->drupalPost($this->getUrl(), $select, 'Save component');
    $this->pass('<pre>' . print_r($this->getUrl(), TRUE) . '</pre>');
    $select_cid = $this->get_cid($this->getUrl());
    $this->pass('<pre>' . print_r($select_cid, TRUE) . '</pre>');

    $this->drupalGet("node/1");
    $this->assertText(t('Price-component'), t('Price component label is visible'));

    $this->assertText(t('Expensive'), t('Price component value option is visible'));
    $this->assertText(t('Cheap'), t('Price component value option is visible'));

    // Add Invoice component to the form
    $invoice = array('add[name]' => 'Invoice-Label', 'add[type]'=>'invoice');
    $this->drupalPost("node/1/webform", $invoice, 'Add');
    $invoice = array('extra[pmid]' => 'invoice', 'extra[price_components][' . $select_cid . ']'=>'200');  // TODO: Have to make sure the index is right
    $this->drupalPost($this->getUrl(), $invoice, 'Save component');
    $invoice_cid = $this->get_cid($this->getUrl());                        // Get the Component ID for this
    $this->drupalGet("node/1/webform/components/" . $invoice_cid);

    // Verify that it all is showing on the form
    $this->drupalGet("node/1");
    $this->assertText(t('Invoice-Label'), t('Invoice label is visible'));

    // Post an entry
    $post_data = array('submitted[price_component]' => '200');
    $this->drupalPost("node/1", $post_data, 'Submit');
    $this->assertText(t('Need a name of the invoice receipient'), 'Warning message displayed that an invoice receipient name is needed.');

    // Add name field to the form and hook it up to the invoice component
    $name = array('add[name]' => 'Name', 'add[type]'=>'textfield');
    $this->drupalPost("node/1/webform", $name, 'Add');                  // Add the component
    $this->drupalPost($this->getUrl(), array(), 'Save component');      // Save with default values
    $name_cid = $this->get_cid($this->getUrl());                        // Get the Component ID for this

    $invoice['extra[other_components][first_name]'] = $name_cid;        // Add to the data array
    $this->drupalPost("node/1/webform/components/" . $invoice_cid, $invoice, 'Save component');  // hook name to the invoice component

    $post_data['submitted[name]'] = $this->randomName(8);              // Populate the name field
    $this->drupalPost("node/1", $post_data, 'Submit');
    $this->assertText(t('Need an e-mail of the invoice receipient'), 'Warning message displayed that an invoice receipient e-mail is needed.');

    // Add the e-mail field to the invoice component and try again..
    $invoice['extra[other_components][mail]'] = '16';        // Add to the data array (cheating right now- dependant on the email field from webform test
    $this->drupalPost("node/1/webform/components/" . $invoice_cid, $invoice, 'Save component');  // hook name to the invoice component

    $this->drupalPost("node/1", $post_data, 'Submit');
    $this->assertText(t('Invoice no 20000 created for '.$post_data['submitted[name]'].'. Total of NOK 200,00'), 'Invoice creation confirmed.');

  }
}