<?php
/**
 * @file
 * Test file for Tripletex integration with Drupal Commerce
 */

/**
* Test payment user interface.
*/
class CommercePaymentTripletexTest extends CommerceBaseTestCase {
  /**
   * Order object.
   */
  protected $order;

  /**
   * Implementation of getInfo().
   */
  public static function getInfo() {
    return array(
      'name' => 'Tripletex Commerce Payment',
      'description' => 'Test the payment tripletex integration.',
      'group' => 'Tripletex',
    );
  }

  /**
   * Implementation of setUp().
   */
  function setUp() {
    $modules = parent::setUpHelper('all');
    $modules[] = 'tripletex_commerce';
    $modules[] = 'tripletex';
    $modules[] = 'tripletex_dummy_api';
    parent::setUp($modules);

    // User creation for different operations.
    $this->store_admin = $this->createStoreAdmin();
    $this->store_customer = $this->createStoreCustomer();
  }

  /**
   * Test an Offsite payment method.
   */
  public function testCommercePaymentTripletexInvoice() {
    // Create a new customer profile.
    $profile = $this->createDummyCustomerProfile('billing', $this->store_customer->uid);
    $profile_wrapper = entity_metadata_wrapper('commerce_customer_profile', $profile);
    // Create an order for store customer.
    $order = $this->createDummyOrder($this->store_customer->uid, array(), 'cart', $profile->profile_id);

    // Login with store admin.
    $this->drupalLogin($this->store_admin);

    // Access to the payment methods administration page.
    $this->drupalGet('admin/commerce/config/payment-methods');

    // Check if the payment method exists and it's listed.
    $this->assertText(t('Electronic invoice'), t('Tripletex invoice payment method is listed in the payment methods administration page'));

    // Login with store customer and access to checkout.
    $this->drupalLogin($this->store_customer);
    $this->drupalGet($this->getCommerceUrl('checkout'));

    // Process the order and check if the offsite payment is working.
    $this->drupalPost(NULL, array(), t('Continue to next step'));
    $this->assertText(t('Electronic invoice'), t('Tripletex invoice payment method is listed in the checkout process form'));
    $this->drupalPostAJAX(NULL, array(  'commerce_payment[payment_method]' => 'tripletex_commerce|commerce_payment_tripletex_commerce'),
                                        'commerce_payment[payment_method]');
    $this->drupalPost(NULL, array(), t('Continue to next step'));

    $this->assertFieldById('edit-continue', t('Continue to next step'), t('Continue to nex step in creating an invoice'));
    // We can't really test further than this.
  }

}
